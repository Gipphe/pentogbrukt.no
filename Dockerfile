FROM node:14.15.4@sha256:04a33dac55af8d3170bffc91ca31fe8000b96ae1bab1a090deb920ca2ca2a38e AS builder
WORKDIR /usr/app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM node:14.15.4@sha256:04a33dac55af8d3170bffc91ca31fe8000b96ae1bab1a090deb920ca2ca2a38e AS exe
WORKDIR /usr/app
RUN groupadd -r appuser && useradd -r -g appuser appuser
COPY --from=builder /usr/app/dist ./dist
RUN npm install -g serve
USER appuser
CMD serve --single --listen 8080 --cors dist
