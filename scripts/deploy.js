import * as ftp from 'basic-ftp';
import * as path from 'node:path';
import { readdir } from 'node:fs/promises';
import * as fs from 'node:fs';

/**
 * Convert a FileInfo object to a Dirent object
 * @param {string} dir The parent directory
 * @param {ftp.FileInfo} info The file info
 * @returns {fs.Dirent} The dirent object
 */
const fileInfoToDirent = (dir) => (info) => {
    return {
        name: info.name,
        isDirectory: () => info.type === 2,
        isFile: () => info.type === 1,
        path: dir,
    };
};

/**
 * List all files in a remote directory and its subdirectories
 * @param {ftp.Client} client The FTP client
 * @param {string} path The path to the directory
 * @returns {Promise<string[]>} A list of all file paths
 **/
const getRemoteDirChildren = (client, path) => {
    return listFilesRecursive(
        (p) => client.list(p).then((xs) => xs.map(fileInfoToDirent(p))),
        path,
    );
};

/**
 * List all files in a local directory and its subdirectories
 * @param {string} path The path to the directory
 * @returns {Promise<string[]>} A list of all file paths
 **/
const getLocalDirChildren = (path) => {
    return listFilesRecursive((p) => readdir(p, { withFileTypes: true }), path);
};

/**
 * List all files in a directory and its subdirectories
 * @param {(path: string) => Promise<fs.Dirent[]>} readDir The readdir function to use.
 * @param {string} path The path to the directory
 * @param {boolean} includeDir Whether to include the directory itself in the list
 * @returns {Promise<string[]>} A list of all file paths
 **/
const listFilesRecursive = async (readDir, path) => {
    const children = await readDir(path);
    const results = [];
    for (const child of children) {
        const childPath = `${path}/${child.name}`;
        if (child.isDirectory()) {
            results.push(...(await listFilesRecursive(readDir, childPath)));
        } else {
            results.push(childPath);
        }
    }
    return results;
};

/**
 * Get the items in setA that are not in setB
 * @param {Iterable<T>} setA The first set
 * @param {Iterable<T>} setB The second set
 * @returns {Set<T>} The difference set
 **/
const setDifference = (setA, setB) => {
    const difference = new Set(setA);
    for (const elem of setB) {
        difference.delete(elem);
    }
    return difference;
};

/**
 * Remove empty directories from the server
 * @param {ftp.Client} client The FTP client
 * @param {string} path The path to the directory
 * @returns {Promise<number>} The number of directories removed
 **/
const removeEmptyDir = async (client, path) => {
    const children = await client
        .list(path)
        .then((xs) => xs.map(fileInfoToDirent(path)));

    if (children.length === 0) {
        await client.removeDir(path);
        return 1;
    }

    let count = 0;
    for (const child of children) {
        if (child.isDirectory()) {
            count += await removeEmptyDir(client, `${path}/${child.name}`);
        }
    }
    return count;
};

const getUsername = () => {
    if (process.env.FTP_USERNAME) {
        return process.env.FTP_USERNAME;
    }
    throw new Error('FTP_USERNAME not set');
};

const getPassword = () => {
    if (process.env.FTP_PASSWORD) {
        return process.env.FTP_PASSWORD;
    }
    throw new Error('FTP_PASSWORD not set');
};

(async () => {
    const sourceDir = './public';
    console.log('Input credentials for Pent og Brukt Domeneshop FTP');
    const user = getUsername();
    const password = getPassword();
    console.log('Connecting...');

    const client = new ftp.Client();

    try {
        await client.access({
            host: 'ftp.domeneshop.no',
            user,
            password,
            secure: true,
        });
        await client.cd('www');
        console.log('Uploading...');
        await client.uploadFromDir(sourceDir);
        const localFiles = await getLocalDirChildren(sourceDir).then((xs) =>
            xs.map((p) =>
                path
                    .relative(sourceDir, p)
                    .replace(path.win32.sep, path.posix.sep),
            ),
        );
        const remoteFiles = await getRemoteDirChildren(client, '/www').then(
            (xs) =>
                xs.map((p) =>
                    path
                        .relative('/www', p)
                        .replace(path.win32.sep, path.posix.sep),
                ),
        );
        const nonMatching = setDifference(remoteFiles, localFiles);
        for (const file of nonMatching) {
            await client.remove(file);
        }
        console.log(`Removed ${nonMatching.size} non-matching files`);
        const emptyDirCount = await removeEmptyDir(client, '/www');
        console.log(`Removed ${emptyDirCount} empty directories`);
        console.log('Deployment complete');
    } catch (e) {
        console.error('Failed');
        console.error(e);
        throw e;
    } finally {
        client.close();
    }
})();
