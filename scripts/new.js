import hugoPath from 'hugo-bin';
import { execFile } from 'child_process';

const title = process.argv[2];
if (!title) {
    process.exit();
}
execFile(
    hugoPath,
    ['new', 'content', `nyheter/${title}/index.md`],
    (error, stdout, stderr) => {
        if (stderr) {
            console.error(stderr);
        }
        if (error) {
            throw error;
        }
        console.log(stdout);
    },
);
