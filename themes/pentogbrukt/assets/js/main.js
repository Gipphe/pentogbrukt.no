const debounce = (ms, f) => {
    let t = null;
    return (...args) => {
        if (t !== null) {
            clearTimeout(t);
        }
        t = setTimeout(() => {
            f(...args);
            t = null;
        }, ms);
    };
};

document.addEventListener(
    'DOMContentLoaded',
    () => {
        const header = document.getElementById('Header');
        if (!header) {
            return;
        }
        const headerSpaceHolder = document.getElementById(
            'Header__SpaceHolder',
        );
        if (!headerSpaceHolder) {
            return;
        }
        const updateHeight = () => {
            headerSpaceHolder.style.height = `${header.offsetHeight}px`;
        };
        updateHeight();

        header.addEventListener('resize', debounce(200, updateHeight), false);
    },
    false,
);
