@echo off

call npm i

echo
echo -------------------
echo
echo Enter the URL slug for the new news post.
echo The URL slug must:
echo - Be unique. No other news post can have the same URL slug.
echo - Be in lower case.
echo - Only contain letters, numbers and hypens.
echo   - Write multiple words by combining them with hyphens.
echo 
echo Example:
echo - my-news-post
echo - new-cupboard
echo - my-unique-news-post-2024-02-27

set /p title="Enter URL slug: "
call npm run new %title%
pause
