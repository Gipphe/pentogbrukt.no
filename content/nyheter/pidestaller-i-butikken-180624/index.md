+++
title = 'Pidestaller I Butikken'
date = 2024-06-19T13:22:41+02:00
+++

Vi har tre lekre pidestaller inne i butikken akkurat nå! De to med apeansikt er i messing, og den med tre etasjer er i lakkert smijern. Disse unike pidestallene er perfekte for å fremheve dine favorittplanter eller dekorative gjenstander.

Kom innom og sjekk dem ut!
