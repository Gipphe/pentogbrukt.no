+++
title = 'Handle Fra Hvor Som Helst'
date = 2024-04-23T18:10:00+02:00
+++

Bor du langt unna, men ønsker likevel å handle hos oss? Ingen problem! Vi sender gjerne varene dine mot at du betaler frakten.

Sjekk oss ut på Tise eller ta kontakt med oss for å avtale sendingen, og betal enkelt via Vipps.
