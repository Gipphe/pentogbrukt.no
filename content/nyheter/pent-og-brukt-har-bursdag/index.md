+++
title = 'Pent Og Brukt Har Bursdag'
date = 2024-04-29T13:28:17+02:00
+++

Vi feirer 3 år med fantastiske tilbud!

Nyt -30% rabatt på alle vaser, lysholdere og kakefat i butikken vår helt frem til 17. mai! Dette tilbudet er perfekt for å sikre deg alt du trenger til alle sommerens sammenkomster! Tilbudet gjelder til og med 16.05.2024, og gjelder ikke stetteboller.

Kom innom og feir med oss!
