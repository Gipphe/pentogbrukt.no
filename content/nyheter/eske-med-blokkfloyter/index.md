+++
title = 'Eske Med Blokkfløyter'
date = 2024-05-19T17:02:03+02:00
+++

Nyhet i butikken! Vi har fått inn en hel pappeske med helt nye og ubrukte blokkfløyter! Perfekt for musikklærere, musikkelskere i alle aldre som ønsker å lære å spille et nytt instrument eller for de som vil utvide sin samling av instrumenter.

Ta turen innom i dag og sikre deg din egen blokkfløyte før de forsvinner!
