+++
title = 'Liten kiste'
date = 2024-03-05T07:00:00+01:00

+++

Denne vakre skatten er perfekt for å legge til en rustikk og sjarm til ethvert rom. Bruk den til oppbevaring av småsaker eller som et dekorativt element i hjemmet ditt. Kom innom butikken vår i dag og oppdag denne nydelige kisten før den forsvinner!
