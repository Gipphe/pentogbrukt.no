+++
title = 'Båtmotorer på lager'
date = 2024-06-20T15:42:58+02:00
+++

På jakt etter en båtmotor? Vi har to båtmotorer på lager akkurat nå! De står ikke lenger i butikken, men spør du etter dem så skal vi hjelpe deg.

Perfekt for sommersesongen!
