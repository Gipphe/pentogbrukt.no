+++
title = 'Vintage sittegruppe'
date = 2024-04-04T15:00:24+02:00
+++

Gi din vinterhage en touch av vintage sjarm! Vi har nettopp flyttet denne fantastiske vintage sittegruppen - komplett med en lenestol, en sofa og et bord i rotting med glassplate - fra retrorommet til inngangspartiet i butikken.

Vi mener den passer perfekt i en vinterhage hvis du har en, og ønsker å gi deg muligheten til å bringe litt retrostil inn i ditt eget uterom. Besøk oss for å se denne tidløse skjønnheten og la deg inspirere til å skape ditt eget vintageparadis!
