+++
title = 'Oppgradert Musikkrom'
date = 2024-02-24T00:00:23+01:00
+++

Musikkrommet vårt har fått en oppgradering! Vi har nå fylt det med enda mer spennende varer som vil glede musikk- og kultur-entusiaster. I tillegg til vårt utvalg av klassiske vinylplater og unike instrumenter, har vi flyttet spill, sangbøker, tegneserier og bilderammer opp dit. Og som om det ikke var nok, har vi til og med laget en krok hvor du kan sette deg ned for å spille av en cd eller ha bakgrunnsmusikk mens du blar gjennom lp-plater, cd`er og filmer.

Musikkrommet finner du ved å gå gjennom Retrorommet, og opp trappen til andre etasje. Utforsk musikkrommet og oppdag nye favoritter hos oss!
