+++
title = 'Farger i vitrineskapet'
date = 2024-04-20T15:00:00+02:00
+++

Se hva vi har gjort med det store hvite vitrineskapet vårt – det er nå fylt til randen med farger! Vi har valgt å legge til farget glass, og resultatet er helt fantastisk! Fargene virkelig popper mot den hvite bakgrunnen og skaper en iøynefallende effekt. Ble ikke dette lekkert?

Ta turen innom butikken for å se selv og la deg inspirere av vårt fargerike utvalg!
