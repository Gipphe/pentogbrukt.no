+++
title = 'Holder stengt 7., 8. og 9 april'
date = 2024-04-03T07:00:15+02:00
+++

Vi tar en liten pause for å gi butikken vår en frisk oppdatering! Derfor vil vi dessverre være stengt den 7., 8. og 9. april.

Men ikke bekymre deg - vi kommer tilbake sterkere enn noensinne med en ommøblert butikk fylt til randen av spennende nyheter! Gled deg til å utforske vår oppdaterte butikk når vi åpner igjen onsdag den 10. april. Vi ser frem til å ønske deg velkommen til en helt ny opplevelse!
