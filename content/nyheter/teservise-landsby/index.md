+++
title = 'Teservise Landsby'
date = 2024-08-25T13:48:27+02:00
+++

Vi har fått inn et helt unikt teservise som er formet og dekorert som en herlig landsby! Hvert stykke er som et lite hus, med detaljer som gir deg følelsen av å være i en sjarmerende liten landsby. Perfekt for deg som elsker det lille ekstra ved te-stunden!

Kom innom og opplev dette fantastiske serviset – det er virkelig noe utenom det vanlige!
