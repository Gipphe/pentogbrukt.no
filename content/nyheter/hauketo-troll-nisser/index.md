+++
title = 'Hauketo Troll Nisser'
date = 2024-07-16T14:34:59+02:00
+++

Er du en fan av Hauketo troll? Da har vi en gladnyhet til deg! Vi har nemlig en fin samling av nisser, troll og en flott skål inne i butikken akkurat nå. Alle produsert av Hauketo Figur Kunst AS.

Kom innom for å finne din favoritt!
