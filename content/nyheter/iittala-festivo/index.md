+++
title = 'Iittala Festivo'
date = 2024-05-25T15:10:20+02:00
+++

Vi har hele 6 like Iittala Festivo lysestaker med to ringer inne akkurat nå! Disse er så dekorative og flotte, og de gjør seg godt i ethvert hjem, nesten uavhengig av din stil! Perfekte for å skape en hyggelig atmosfære.

Kom innom og sikre deg en eller flere av disse tidløse skjønnhetene!
