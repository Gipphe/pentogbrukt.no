+++
title = 'Alf Prøysen Samletallerkner'
date = 2024-05-09T17:31:21+02:00
+++

Utforsk vår samling av 6 ulike samletallerkner i Alf Prøysen-serien, laget i et begrenset opplag i 2000! Disse tallerkene er en perfekt måte å bringe litt nostalgisk sjarm til ditt bord. Fra Prøysens kjente figurer til ikoniske sitater, det er noe for enhver smak. Kom innom og bli inspirert av Prøysens univers!
