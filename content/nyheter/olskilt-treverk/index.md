+++
title = 'Ølskilt'
date = 2024-04-11T15:00:00+02:00
+++

Skål til ølelskere! Vi har nå fått inn to flotte ølskilt som vil sette prikken over i-en i enhver mannehule eller hjemmebar! Fra det ikoniske store Ringnes skiltet til det lille klassiske Carlsberg skiltet - disse vil gi enhver vegg karakter. Perfekt for å skape den rette atmosfæren for en avslappende stund med venner eller en deilig hjemmebrygget øl.

Ta turen innom butikken og se disse flotte skiltene med egne øyne!
