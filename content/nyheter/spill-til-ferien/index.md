+++
title = 'Spill Til Ferien'
date = 2024-03-21T13:00:10+01:00
+++

Påskeferien er perfekt for brettspill og puslespill moro!

Vi har et bredt utvalg av både brettspill og puslespill som vil gjøre ferien uforglemmelig- noen av dem er til og med helt uåpnede brettspill med intakt forsegling! Enten du foretrekker å utfordre familie og venner med et spennende brettspill eller foretrekker å dykke ned i en verden av puslespill, har vi noe for enhver smak.

Ta turen innom butikken vår for å se det enda større utvalget vårt!
