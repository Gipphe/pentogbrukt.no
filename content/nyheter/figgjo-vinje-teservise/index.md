+++
title = 'Figgjo Vinje teservise'
date = 2024-03-18T17:00:00+01:00
+++

Oppdag elegansen i Vinje-serien fra Figgjo! Vi har nå fått inn nyeste dette nydelige teserviset i serien, som vil løfte din te-stund til nye høyder. Med sitt klassiske design er dette serviset perfekt for både hverdag og festlige anledninger.

Ta turen innom og la deg fortrylle av skjønnheten i Vinje-serien - din te-stund fortjener det beste!
