+++
title = 'Lillestrom Lampe'
date = 2024-09-21T11:11:33+02:00
+++

Vi har fått inn en unik Lillestrøm-lampe i butikken! Den firkantede lampeskjermen er dekorert med fire forskjellige motiver av bygninger fra Lillestrøm, noe som gjør den både nostalgisk og helt spesiell. Perfekt for deg som har et hjerte for byen!

Kom innom og ta en titt – denne må bare oppleves.
