+++
title = 'Kurvstol på eksternt lager'
date = 2024-03-15T18:00:00+01:00
+++

Oppdag vår nyeste skatt på vårt eksterne lager - en vakker kurvstol som vil legge til et snev av bohemsk eleganse i ethvert rom! Denne lekre kurvstolen er perfekt for å skape en avslappende og innbydende atmosfære hjemme.

Kr. 500,-

Ta kontakt med oss for å avtale, så sørger vi for å ta den med til butikken for deg. Ikke gå glipp av sjansen til å legge til denne tidløse klassikeren i ditt interiør!
