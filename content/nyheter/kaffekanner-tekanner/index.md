+++
title = 'Kaffekanner og Tekanner'
date = 2024-07-24T17:29:37+02:00
+++

Har du sett oppå det store skapet med glass i butikken vår? Der har vi samlet de fleste av våre kaffe- og te-kanner. Enten du er på utkikk etter en vintage kaffekanne eller en moderne tekanne, så har vi et bredt utvalg å velge mellom.

Besøk oss og la deg inspirere av vårt unike utvalg av kanner til din neste kaffestund!
