+++
title = 'Nye Tilskudd Til Retrorommet'
date = 2024-04-14T14:00:00+02:00
+++

Retrorommet vårt har fått enda flere spennende tilskudd! Ta en titt på vårt nye skattoll, flere stoler og en majestetisk figur som har funnet veien til butikken vår.

Utforsk det nostalgiske og unike utvalget av retro varer på retrorommet – det er alltid noe spennende å oppdage!
