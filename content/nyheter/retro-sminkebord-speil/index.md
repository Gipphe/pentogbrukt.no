+++
title = 'Retro Sminkebord og Speil'
date = 2024-07-21T16:47:22+02:00
+++

Vi har fått inn et lekkert retro sett med et stort speil og et sminkebord i lakkert hvitt metall og en rød, stilig bordplate. Perfekt for deg som elsker vintage stil og vil tilføre noe unikt til hjemmet ditt.

Kom og se dette flotte settet før det blir borte!
