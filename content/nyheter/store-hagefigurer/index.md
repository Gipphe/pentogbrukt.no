+++
title = 'Store Hagefigurer'
date = 2024-06-18T10:17:57+02:00
+++

Akkurat nå har vi tre store hagefigurer og et stort fuglebad inne i butikken! De to store hvite englene har dessverre noen skader, men de er fortsatt nydelige og skadene synes kun om du ser dem på nært hold.

Perfekte til å gi hagen din en unik og vakker touch!
