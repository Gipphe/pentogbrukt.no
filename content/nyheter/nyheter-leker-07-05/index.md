+++
title = 'Nyheter blandt lekene'
date = 2024-05-07T12:24:40+02:00
+++

Sjekk ut vår lekeavdeling for noen ferske tilskudd! Vi har nettopp fått inn noen spennende nye leker, inkludert en retro Fisher Price leke-TV! Det er en nostalgisk perle som garantert vil bringe tilbake gode minner.
Ta med familien og utforsk alle de morsomme lekene vi har å tilby!
