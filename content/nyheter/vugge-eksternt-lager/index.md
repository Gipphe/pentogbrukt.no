+++
title = 'Vugge på eksternt lager'
date = 2024-03-11T17:00:00+01:00
+++

På vårt eksterne lager har vi nå en vakker vugge i tre med nydelige utskjæringer! Denne sjarmerende vuggen er perfekt for å skape en koselig og trygg atmosfære for din lille skatt.

Pris: Kr. 1750,-

Vuggens mål er følgende:
B: 96 cm
D: 69 cm
H: 71 cm

Ta kontakt med oss for å avtale, så kan vi ta den med til butikken for deg. Og husk, vi har også en rekke andre skatter på vårt eksterne lager, så hvis du leter etter noe spesielt, ikke nøl med å spørre oss!
