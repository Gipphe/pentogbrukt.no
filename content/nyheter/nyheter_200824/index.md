+++
title = 'Nyheter i butikken'
date = 2024-08-20T09:51:07+02:00
+++

Den siste tiden har vi fått inn masse spennende i butikken! Blant annet har vi nå noen lekre små samlegitarer, nydelige ferskenfargede glass, elegante bordklokker, mummiglass og mye, mye mer!

Kom innom og ta en titt – her finner du garantert noe du vil like!
