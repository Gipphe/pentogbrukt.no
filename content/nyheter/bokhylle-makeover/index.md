+++
title = 'Bokhylle Makeover'
date = 2024-02-28T15:00:00+01:00
+++

Vi har gitt bokhyllen vår en skikkelig makeover, og nå er hyllene kategorisert og mer oversiktlige enn noensinne! Enten du er interessert i kunst, historie, eller annen spennende lesning, har vi bøker som vil fange interessen din. Og det er ikke alt - vi har også flyttet tegneseriene opp til musikkrommet og kokebøkene til kjøkkenavdelingen for en enda bedre handleopplevelse!

Kom innom butikken vår i dag og oppdag vårt nye og forbedrede bokutvalg!
