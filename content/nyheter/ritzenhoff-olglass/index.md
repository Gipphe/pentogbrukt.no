+++
title = 'Ritzenhoff Ølglass'
date = 2024-05-29T14:52:00+02:00
+++

Er du en samler av Ritzenhoff-glass?

Vi har to ulike glass fra denne ikoniske produsenten inne i butikken akkurat nå! Disse glassene er ikke bare perfekte til å nyte favorittølet ditt, de er også ettertraktede samleobjekter. Hvert år lanseres det en limited edition kolleksjon med ulike design, og vi har noen fra et tidligere år på lager.

Kom innom og ta en titt – kanskje du finner akkurat det ene glasset du mangler.
