+++
title = 'Rosa måneskinnslampe'
date = 2024-03-08T18:00:00+01:00
+++

Sjekk ut vår nyeste tilskudd: denne fantastiske måneskinnslampen! Denne nydelige rosa lampen skaper en atmosfære av magi og mystikk i ethvert rom. Det beste av alt? Den trenger ikke strøm! Laget for å bruke stearinlys, er denne lampen både praktisk og vakker.
Besøk oss i butikken for å ta en nærmere titt på denne unike skatten!
