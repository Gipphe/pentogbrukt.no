+++
title = 'Musikkoase Musikk Radio'
date = 2024-04-10T13:00:02+02:00
+++

Utforsk vår musikalske oase i 2. etasje!
Vår musikkavdeling er spekket med skatter som vil glede enhver musikkentusiast. Her finner du alt fra brettspill og musikkinstrumenter til CD-er, DVD-er, VHS, LP-plater, kassetter, bilderammer og tegneserier. Og som om det ikke var nok, har vi nå også fått inn en gammel radio, høyttalere, høyttalerstativ og et skap dedikert til forsterkere og lignende.
Ta turen gjennom retrorommet og opp trappen i gangen og la deg fortrylle av vårt varierte utvalg!
