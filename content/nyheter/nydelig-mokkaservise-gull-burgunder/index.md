+++
title = 'Nydelig mokkaservise i burgunder'
date = 2024-04-21T11:00:00+02:00
+++

Vi har akkurat fått inn et nydelig mokkaservise i burgunder og grønt, med delikate gulldetaljer som gir det et tidløst preg. Perfekt for å skape en stemningsfull kaffestund eller imponere gjestene dine med en elegant servering.

Ta turen innom butikken og la deg inspirere av denne vakre skatten!
