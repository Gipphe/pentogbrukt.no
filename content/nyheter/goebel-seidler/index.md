+++
title = 'Goebel Seidler'
date = 2024-04-08T18:00:32+02:00
+++

Skap litt lastebil-estetikk i ditt hjem med våre Goebel-seidler!

Vi har akkurat nå flere flotte seidler fra Goebel, hver inngravert med MAN og utsmykket med sjarmerende motiver av lastebiler. Perfekt for lastebil-entusiaster eller som en unik gave til en venn med lidenskap for veien.
