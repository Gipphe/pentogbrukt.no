+++
title = 'Brother strikkemaskin på eksternt lager'
date = 2024-03-06T12:00:00+01:00
+++

Akkurat nå har vi en spennende nyhet for alle strikkeentusiaster der ute!

Vi har en flott strikkemaskin fra Brother tilgjengelig på vårt eksterne lager. Den kan tas med til butikken etter avtale, slik at du kan få utforske alle de fantastiske mulighetene denne maskinen har å tilby. Ikke gå glipp av sjansen til å ta din strikking til et helt nytt nivå!

Pris: Kr. 2000,-

Maskinen har følgende mål:
B: 170 cm
H: 105 cm
D: 87 cm

Husk også at vi har en rekke andre varer på eksternt lager, så spør gjerne om du ser etter noe spesielt. Ta kontakt med oss for å avtale nærmere detaljer.
