+++
title = 'Rustikk keramikkservise'
date = 2024-04-13T16:00:00+02:00
+++

Gi middagsbordet ditt et snev av rustikk eleganse med vårt nydelige servise i grå-brun keramikk! Dette settet inneholder en fantastisk stor suppeterrin, perfekt for servering av deilige supper og gryteretter. I tillegg tilbyr vi dype skåler og glass som supplerer serviset perfekt.

Ta turen innom butikken vår og legg til dette tidløse serviset i hjemmeinnredningen din.
