+++
title = 'Klara Hadeland Flat Champagne'
date = 2024-06-25T13:01:21+02:00
+++

Vi har fått inn 11 stk. Klara flate champagneglass fra Hadeland, og de er dekorert med nydelig mønster! Disse glassene er perfekte for å skåle i stil til enhver anledning. 

Kom innom og sikre deg disse elegante og tidløse glassene til ditt hjem!
