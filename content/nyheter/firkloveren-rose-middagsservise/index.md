+++
title = 'Firkløveren Rose middagsservise'
date = 2024-09-13T14:37:25+02:00
+++

Akkurat nå har vi et nydelig middagsservise i serien Rose fra Firkløveren inne i butikken.
Har du sett noe så lekkert? Perfekt for å dekke et elegant bord til selskapet eller familiemiddagen!
