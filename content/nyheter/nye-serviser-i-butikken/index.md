+++
title = 'Nye Serviser I Butikken'
date = 2024-06-28T09:58:33+02:00
+++

I det siste har vi fått inn flere ulike serviser i butikken! Blant annet har vi fått inn flere serviser fra Porsgrund Porselen.

Kom innom og finn ditt nye favorittservise hos oss! Perfekt til å imponere gjestene dine eller bare for å nyte et måltid i stil.
