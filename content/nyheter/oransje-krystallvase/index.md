+++
title = 'Oransje Krystallvase'
date = 2024-06-07T16:03:08+02:00
+++

Butikkens daglige leder har en absolutt favoritt i butikken akkurat nå, en stor og nydelig oransje krystallvase med flotte detaljer.

-   Jeg synes denne vasen er så majestetisk og utrolig vakker, og den fargen da. Egentlig har jeg lyst på den selv!

Ta turen innom og se om den blir din favoritt også!
