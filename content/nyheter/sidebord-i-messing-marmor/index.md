+++
title = 'Sidebord i messing og marmor'
date = 2024-04-28T11:39:32+02:00
+++

Vi har et stort utvalg av småbord inne i butikken akkurat nå, inkludert dette nydelige sidebordet i messing med bordplate i mørk marmor. Det er virkelig en perle! Perfekt for å legge til litt glam i ethvert rom.

Kom innom og sjekk ut vårt utvalg av småbord i dag!
