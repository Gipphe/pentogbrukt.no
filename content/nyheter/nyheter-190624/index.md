+++
title = 'Nyheter i butikken'
date = 2024-06-19T09:10:35+02:00
+++

Vi har fått inn en mengde nyheter den siste tiden! Blant skattene finner du et gammelt telefonskilt, elegante buljongkopper, en praktfull lysekrone, og champagneglass uten stett - perfekte til bryllup eller andre spesielle feiringer.

Kom innom og se alle de nye flotte varene vi har inne nå!
