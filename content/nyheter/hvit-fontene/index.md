+++
title = 'Hvit Fontene'
date = 2024-08-08T18:00:05+02:00
+++

Vi har fått inn en helt nydelig fontene med lekker dekorasjon og lampe over! Denne skjønnheten selges for seg selv, men pidestallen den står på passer perfekt til den. Vi anbefaler å kjøpe begge to sammen for å fullføre stilen.

Kom innom og se hvor flott denne fontenen er – den vil garantert bli et blikkfang i ditt hjem!
