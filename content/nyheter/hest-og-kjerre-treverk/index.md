+++
title = 'Hest Og Kjerre I Treverk'
date = 2024-06-02T14:28:44+02:00
+++

Vi har fått inn en sjarmerende gammel hest og kjerre figur i treverk! Den har noe slitasje og mangler, men er fortsatt et flott blikkfang og full av karakter. Perfekt for å gi hjemmet ditt en touch av nostalgi og historie!
