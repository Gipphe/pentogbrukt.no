+++
title = 'Moderne Dekor'
date = 2024-08-08T09:37:34+02:00
+++

Vi har fått inn litt nyheter i butikken og fyller stadig opp med mer fremover! Blant annet har vi nå fått inn en del moderne dekor. Sjekk ut de lekre sorte leopardformede kubbelysholderne, en stor giraff-figur, ulike elementer i messing, en nesehornfigur og mye annet spennende!

Kom innom og finn dine nye favoritter blant våre nyheter – vi gleder oss til å se deg!
