+++
title = 'Ikea Lindmon Persienner'
date = 2024-06-21T11:38:12+02:00
+++

Vi har fått inn tre helt nye persienner fra IKEA i serien Lindmon! De måler 100 cm x 250 cm og kommer i originaleskene. Perfekte for å friske opp hjemmet ditt med stil!🪟

Kom innom og ta en titt før de blir borte!
