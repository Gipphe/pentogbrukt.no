+++
title = 'Nyheter I Butikk'
date = 2024-05-30T11:38:44+02:00
+++

Vi har fått inn mange nyheter i butikken nå! Sjekk ut det nye mokkaserviset, en maritim-inspirert karaffel, et nydelig retro sminkebord med tilhørende speil, flotte fruktasjetter, en lekker rustikk vegghylle og mye annet spennende!

Kom innom og oppdag våre nyeste skatter!
