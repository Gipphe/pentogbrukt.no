+++
title = 'Vekter'
date = 2024-07-02T11:49:53+02:00
+++

Vi har en rekke vekter av ulike typer inne i butikken akkurat nå! Både digitale og analoge varianter, samt en sjarmerende babyvekt perfekt for å følge med på de smås vektøkning.⚖️
Kom innom oss for å se hele utvalget og finn den vekten som passer dine behov best!
