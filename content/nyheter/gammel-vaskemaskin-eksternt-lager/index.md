+++
title = 'Gammel vaskemaskin på eksternt lager'
date = 2024-03-27T14:00:00+01:00
+++

På jakt etter noe unikt med et snev av nostalgisk sjarm? Da har vi denne gamle vaskemaskinen i treverk med sveiv på vårt eksterne lager!

Denne sjarmerende maskinen vil ikke bare være et praktisk tilskudd til hjemmet ditt, men også et estetisk tiltalende element som kan gi en følelse av gammeldags sjarme til ethvert rom.

Kr. 750,-

Ta kontakt med oss hvis du ønsker at vi skal ta den med til butikken, og gi hjemmet ditt det lille ekstra preg av historie og karakter!
