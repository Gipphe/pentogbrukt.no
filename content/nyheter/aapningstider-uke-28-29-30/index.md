+++
title = 'Åpningstider Uke 28, 29, 30'
date = 2024-06-24T20:08:22+02:00
+++

Sommerens åpningstider hos Pent og Brukt AS

I uke 28, 29 og 30 har vi litt sommerferie i butikken, og vi holder derfor åpent færre dager og har kortere åpningstider ellers i uken.
Disse åpningstidene gjelder kun disse tre ukene, og vi holder åpent som normalt alle andre dager bortsett fra i morgen, tirsdag 25. juni - hvor vi åpner kl. 13.00. Vi håper dere tar turen innom oss i åpningstidene og vi håper alltid at dere finner noe fint som passer til akkurat deg! God sommer!
