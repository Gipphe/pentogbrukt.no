const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './hugo_stats.json',
        './layouts/**/*.html',
        './content/**/*.md',
        './content/**/*.html',
        './themes/**/layouts/**/*.html',
        './themes/**/content/**/*.md',
        './themes/**/content/**/*.html',
    ],
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                current: 'currentColor',
                white: '#ffffff',
                teal: {
                    light: '#DEECED',
                    DEFAULT: '#B6CED0',
                    dark: '#5D7D7F',
                },
                black: '#000000',
                gray: {
                    DEFAULT: 'rgba(0, 0, 0, 0.11)',
                    dark: '#707070',
                },
                green: '#5C9F5A',
            },
            spacing: {
                content: '1200px',
                'mobile-margin': '1.25rem',
            },
        },
        fontFamily: {
            ...defaultTheme.fontFamily,
            sans: ['"Quicksand"', ...defaultTheme.fontFamily.sans],
        },
    },
    plugins: [],
};
